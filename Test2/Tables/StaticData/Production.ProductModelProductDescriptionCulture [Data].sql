-- Foreign Key Constraint Disable's for Table: [Production].[ProductModelProductDescriptionCulture]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductModelProductDescriptionCulture]'
ALTER TABLE [Production].[ProductModelProductDescriptionCulture] NOCHECK CONSTRAINT [FK_ProductModelProductDescriptionCulture_Culture_CultureID]
ALTER TABLE [Production].[ProductModelProductDescriptionCulture] NOCHECK CONSTRAINT [FK_ProductModelProductDescriptionCulture_ProductDescription_ProductDescriptionID]
ALTER TABLE [Production].[ProductModelProductDescriptionCulture] NOCHECK CONSTRAINT [FK_ProductModelProductDescriptionCulture_ProductModel_ProductModelID]


-- No rows are in ProductModelProductDescriptionCulture
PRINT 'No rows are in ProductModelProductDescriptionCulture'

