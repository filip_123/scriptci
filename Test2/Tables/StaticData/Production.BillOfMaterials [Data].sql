-- Check Constraint Disable's for Table: [Production].[BillOfMaterials]
Print 'Check Constraint Disable''s for Table: [Production].[BillOfMaterials]'
ALTER TABLE [Production].[BillOfMaterials] NOCHECK CONSTRAINT [CK_BillOfMaterials_BOMLevel]
ALTER TABLE [Production].[BillOfMaterials] NOCHECK CONSTRAINT [CK_BillOfMaterials_EndDate]
ALTER TABLE [Production].[BillOfMaterials] NOCHECK CONSTRAINT [CK_BillOfMaterials_PerAssemblyQty]
ALTER TABLE [Production].[BillOfMaterials] NOCHECK CONSTRAINT [CK_BillOfMaterials_ProductAssemblyID]

-- Foreign Key Constraint Disable's for Table: [Production].[BillOfMaterials]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[BillOfMaterials]'
ALTER TABLE [Production].[BillOfMaterials] NOCHECK CONSTRAINT [FK_BillOfMaterials_Product_ComponentID]
ALTER TABLE [Production].[BillOfMaterials] NOCHECK CONSTRAINT [FK_BillOfMaterials_Product_ProductAssemblyID]
ALTER TABLE [Production].[BillOfMaterials] NOCHECK CONSTRAINT [FK_BillOfMaterials_UnitMeasure_UnitMeasureCode]


-- No rows are in BillOfMaterials
PRINT 'No rows are in BillOfMaterials'

