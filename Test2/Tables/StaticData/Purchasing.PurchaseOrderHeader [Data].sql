-- Check Constraint Disable's for Table: [Purchasing].[PurchaseOrderHeader]
Print 'Check Constraint Disable''s for Table: [Purchasing].[PurchaseOrderHeader]'
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [CK_PurchaseOrderHeader_Freight]
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [CK_PurchaseOrderHeader_ShipDate]
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [CK_PurchaseOrderHeader_Status]
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [CK_PurchaseOrderHeader_SubTotal]
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [CK_PurchaseOrderHeader_TaxAmt]

-- Foreign Key Constraint Disable's for Table: [Purchasing].[PurchaseOrderHeader]
Print 'Foreign Key Constraint Disable''s for Table: [Purchasing].[PurchaseOrderHeader]'
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [FK_PurchaseOrderHeader_Employee_EmployeeID]
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [FK_PurchaseOrderHeader_ShipMethod_ShipMethodID]
ALTER TABLE [Purchasing].[PurchaseOrderHeader] NOCHECK CONSTRAINT [FK_PurchaseOrderHeader_Vendor_VendorID]

-- Trigger Disable's for Table: [Purchasing].[PurchaseOrderHeader]
Print 'Trigger Disable''s for Table: [Purchasing].[PurchaseOrderHeader]'
ALTER TABLE [Purchasing].[PurchaseOrderHeader] DISABLE TRIGGER [uPurchaseOrderHeader]


-- No rows are in PurchaseOrderHeader
PRINT 'No rows are in PurchaseOrderHeader'

