-- Foreign Key Constraint Disable's for Table: [Person].[StateProvince]
Print 'Foreign Key Constraint Disable''s for Table: [Person].[StateProvince]'
ALTER TABLE [Person].[StateProvince] NOCHECK CONSTRAINT [FK_StateProvince_CountryRegion_CountryRegionCode]
ALTER TABLE [Person].[StateProvince] NOCHECK CONSTRAINT [FK_StateProvince_SalesTerritory_TerritoryID]


-- No rows are in StateProvince
PRINT 'No rows are in StateProvince'

