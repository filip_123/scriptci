-- Check Constraint Disable's for Table: [Sales].[SalesOrderHeader]
Print 'Check Constraint Disable''s for Table: [Sales].[SalesOrderHeader]'
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [CK_SalesOrderHeader_DueDate]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [CK_SalesOrderHeader_Freight]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [CK_SalesOrderHeader_ShipDate]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [CK_SalesOrderHeader_Status]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [CK_SalesOrderHeader_SubTotal]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [CK_SalesOrderHeader_TaxAmt]

-- Foreign Key Constraint Disable's for Table: [Sales].[SalesOrderHeader]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesOrderHeader]'
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_Address_BillToAddressID]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_Address_ShipToAddressID]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_CreditCard_CreditCardID]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_CurrencyRate_CurrencyRateID]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_Customer_CustomerID]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_SalesPerson_SalesPersonID]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_SalesTerritory_TerritoryID]
ALTER TABLE [Sales].[SalesOrderHeader] NOCHECK CONSTRAINT [FK_SalesOrderHeader_ShipMethod_ShipMethodID]

-- Trigger Disable's for Table: [Sales].[SalesOrderHeader]
Print 'Trigger Disable''s for Table: [Sales].[SalesOrderHeader]'
ALTER TABLE [Sales].[SalesOrderHeader] DISABLE TRIGGER [uSalesOrderHeader]


-- No rows are in SalesOrderHeader
PRINT 'No rows are in SalesOrderHeader'

