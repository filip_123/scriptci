-- Check Constraint Disable's for Table: [Production].[ProductInventory]
Print 'Check Constraint Disable''s for Table: [Production].[ProductInventory]'
ALTER TABLE [Production].[ProductInventory] NOCHECK CONSTRAINT [CK_ProductInventory_Bin]
ALTER TABLE [Production].[ProductInventory] NOCHECK CONSTRAINT [CK_ProductInventory_Shelf]

-- Foreign Key Constraint Disable's for Table: [Production].[ProductInventory]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductInventory]'
ALTER TABLE [Production].[ProductInventory] NOCHECK CONSTRAINT [FK_ProductInventory_Location_LocationID]
ALTER TABLE [Production].[ProductInventory] NOCHECK CONSTRAINT [FK_ProductInventory_Product_ProductID]


-- No rows are in ProductInventory
PRINT 'No rows are in ProductInventory'

