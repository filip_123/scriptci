-- Foreign Key Constraint Disable's for Table: [Sales].[SalesOrderHeaderSalesReason]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesOrderHeaderSalesReason]'
ALTER TABLE [Sales].[SalesOrderHeaderSalesReason] NOCHECK CONSTRAINT [FK_SalesOrderHeaderSalesReason_SalesOrderHeader_SalesOrderID]
ALTER TABLE [Sales].[SalesOrderHeaderSalesReason] NOCHECK CONSTRAINT [FK_SalesOrderHeaderSalesReason_SalesReason_SalesReasonID]


-- No rows are in SalesOrderHeaderSalesReason
PRINT 'No rows are in SalesOrderHeaderSalesReason'

