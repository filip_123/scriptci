-- Check Constraint Disable's for Table: [HumanResources].[EmployeeDepartmentHistory]
Print 'Check Constraint Disable''s for Table: [HumanResources].[EmployeeDepartmentHistory]'
ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] NOCHECK CONSTRAINT [CK_EmployeeDepartmentHistory_EndDate]

-- Foreign Key Constraint Disable's for Table: [HumanResources].[EmployeeDepartmentHistory]
Print 'Foreign Key Constraint Disable''s for Table: [HumanResources].[EmployeeDepartmentHistory]'
ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] NOCHECK CONSTRAINT [FK_EmployeeDepartmentHistory_Department_DepartmentID]
ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] NOCHECK CONSTRAINT [FK_EmployeeDepartmentHistory_Employee_BusinessEntityID]
ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] NOCHECK CONSTRAINT [FK_EmployeeDepartmentHistory_Shift_ShiftID]


-- No rows are in EmployeeDepartmentHistory
PRINT 'No rows are in EmployeeDepartmentHistory'

