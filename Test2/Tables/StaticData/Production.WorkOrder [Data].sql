-- Check Constraint Disable's for Table: [Production].[WorkOrder]
Print 'Check Constraint Disable''s for Table: [Production].[WorkOrder]'
ALTER TABLE [Production].[WorkOrder] NOCHECK CONSTRAINT [CK_WorkOrder_EndDate]
ALTER TABLE [Production].[WorkOrder] NOCHECK CONSTRAINT [CK_WorkOrder_OrderQty]
ALTER TABLE [Production].[WorkOrder] NOCHECK CONSTRAINT [CK_WorkOrder_ScrappedQty]

-- Foreign Key Constraint Disable's for Table: [Production].[WorkOrder]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[WorkOrder]'
ALTER TABLE [Production].[WorkOrder] NOCHECK CONSTRAINT [FK_WorkOrder_Product_ProductID]
ALTER TABLE [Production].[WorkOrder] NOCHECK CONSTRAINT [FK_WorkOrder_ScrapReason_ScrapReasonID]

-- Trigger Disable's for Table: [Production].[WorkOrder]
Print 'Trigger Disable''s for Table: [Production].[WorkOrder]'
ALTER TABLE [Production].[WorkOrder] DISABLE TRIGGER [uWorkOrder]
ALTER TABLE [Production].[WorkOrder] DISABLE TRIGGER [iWorkOrder]


-- No rows are in WorkOrder
PRINT 'No rows are in WorkOrder'

