-- Check Constraint Disable's for Table: [Purchasing].[PurchaseOrderDetail]
Print 'Check Constraint Disable''s for Table: [Purchasing].[PurchaseOrderDetail]'
ALTER TABLE [Purchasing].[PurchaseOrderDetail] NOCHECK CONSTRAINT [CK_PurchaseOrderDetail_OrderQty]
ALTER TABLE [Purchasing].[PurchaseOrderDetail] NOCHECK CONSTRAINT [CK_PurchaseOrderDetail_ReceivedQty]
ALTER TABLE [Purchasing].[PurchaseOrderDetail] NOCHECK CONSTRAINT [CK_PurchaseOrderDetail_RejectedQty]
ALTER TABLE [Purchasing].[PurchaseOrderDetail] NOCHECK CONSTRAINT [CK_PurchaseOrderDetail_UnitPrice]

-- Foreign Key Constraint Disable's for Table: [Purchasing].[PurchaseOrderDetail]
Print 'Foreign Key Constraint Disable''s for Table: [Purchasing].[PurchaseOrderDetail]'
ALTER TABLE [Purchasing].[PurchaseOrderDetail] NOCHECK CONSTRAINT [FK_PurchaseOrderDetail_Product_ProductID]
ALTER TABLE [Purchasing].[PurchaseOrderDetail] NOCHECK CONSTRAINT [FK_PurchaseOrderDetail_PurchaseOrderHeader_PurchaseOrderID]

-- Trigger Disable's for Table: [Purchasing].[PurchaseOrderDetail]
Print 'Trigger Disable''s for Table: [Purchasing].[PurchaseOrderDetail]'
ALTER TABLE [Purchasing].[PurchaseOrderDetail] DISABLE TRIGGER [iPurchaseOrderDetail]
ALTER TABLE [Purchasing].[PurchaseOrderDetail] DISABLE TRIGGER [uPurchaseOrderDetail]


-- No rows are in PurchaseOrderDetail
PRINT 'No rows are in PurchaseOrderDetail'

