-- Foreign Key Constraint Disable's for Table: [Sales].[Customer]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[Customer]'
ALTER TABLE [Sales].[Customer] NOCHECK CONSTRAINT [FK_Customer_Person_PersonID]
ALTER TABLE [Sales].[Customer] NOCHECK CONSTRAINT [FK_Customer_SalesTerritory_TerritoryID]
ALTER TABLE [Sales].[Customer] NOCHECK CONSTRAINT [FK_Customer_Store_StoreID]


-- No rows are in Customer
PRINT 'No rows are in Customer'

