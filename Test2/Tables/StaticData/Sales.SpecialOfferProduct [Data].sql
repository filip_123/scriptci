-- Foreign Key Constraint Disable's for Table: [Sales].[SpecialOfferProduct]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SpecialOfferProduct]'
ALTER TABLE [Sales].[SpecialOfferProduct] NOCHECK CONSTRAINT [FK_SpecialOfferProduct_Product_ProductID]
ALTER TABLE [Sales].[SpecialOfferProduct] NOCHECK CONSTRAINT [FK_SpecialOfferProduct_SpecialOffer_SpecialOfferID]


-- No rows are in SpecialOfferProduct
PRINT 'No rows are in SpecialOfferProduct'

