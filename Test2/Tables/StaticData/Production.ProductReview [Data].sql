-- Check Constraint Disable's for Table: [Production].[ProductReview]
Print 'Check Constraint Disable''s for Table: [Production].[ProductReview]'
ALTER TABLE [Production].[ProductReview] NOCHECK CONSTRAINT [CK_ProductReview_Rating]

-- Foreign Key Constraint Disable's for Table: [Production].[ProductReview]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductReview]'
ALTER TABLE [Production].[ProductReview] NOCHECK CONSTRAINT [FK_ProductReview_Product_ProductID]


-- No rows are in ProductReview
PRINT 'No rows are in ProductReview'

