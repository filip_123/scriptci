-- Foreign Key Constraint Disable's for Table: [Sales].[CountryRegionCurrency]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[CountryRegionCurrency]'
ALTER TABLE [Sales].[CountryRegionCurrency] NOCHECK CONSTRAINT [FK_CountryRegionCurrency_CountryRegion_CountryRegionCode]
ALTER TABLE [Sales].[CountryRegionCurrency] NOCHECK CONSTRAINT [FK_CountryRegionCurrency_Currency_CurrencyCode]


-- No rows are in CountryRegionCurrency
PRINT 'No rows are in CountryRegionCurrency'

