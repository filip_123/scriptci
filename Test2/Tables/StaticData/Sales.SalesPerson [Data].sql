-- Check Constraint Disable's for Table: [Sales].[SalesPerson]
Print 'Check Constraint Disable''s for Table: [Sales].[SalesPerson]'
ALTER TABLE [Sales].[SalesPerson] NOCHECK CONSTRAINT [CK_SalesPerson_Bonus]
ALTER TABLE [Sales].[SalesPerson] NOCHECK CONSTRAINT [CK_SalesPerson_CommissionPct]
ALTER TABLE [Sales].[SalesPerson] NOCHECK CONSTRAINT [CK_SalesPerson_SalesLastYear]
ALTER TABLE [Sales].[SalesPerson] NOCHECK CONSTRAINT [CK_SalesPerson_SalesQuota]
ALTER TABLE [Sales].[SalesPerson] NOCHECK CONSTRAINT [CK_SalesPerson_SalesYTD]

-- Foreign Key Constraint Disable's for Table: [Sales].[SalesPerson]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesPerson]'
ALTER TABLE [Sales].[SalesPerson] NOCHECK CONSTRAINT [FK_SalesPerson_Employee_BusinessEntityID]
ALTER TABLE [Sales].[SalesPerson] NOCHECK CONSTRAINT [FK_SalesPerson_SalesTerritory_TerritoryID]


-- No rows are in SalesPerson
PRINT 'No rows are in SalesPerson'

