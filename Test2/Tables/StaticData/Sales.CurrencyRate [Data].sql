-- Foreign Key Constraint Disable's for Table: [Sales].[CurrencyRate]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[CurrencyRate]'
ALTER TABLE [Sales].[CurrencyRate] NOCHECK CONSTRAINT [FK_CurrencyRate_Currency_FromCurrencyCode]
ALTER TABLE [Sales].[CurrencyRate] NOCHECK CONSTRAINT [FK_CurrencyRate_Currency_ToCurrencyCode]


-- No rows are in CurrencyRate
PRINT 'No rows are in CurrencyRate'

