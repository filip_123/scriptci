-- Check Constraint Disable's for Table: [Production].[ProductListPriceHistory]
Print 'Check Constraint Disable''s for Table: [Production].[ProductListPriceHistory]'
ALTER TABLE [Production].[ProductListPriceHistory] NOCHECK CONSTRAINT [CK_ProductListPriceHistory_EndDate]
ALTER TABLE [Production].[ProductListPriceHistory] NOCHECK CONSTRAINT [CK_ProductListPriceHistory_ListPrice]

-- Foreign Key Constraint Disable's for Table: [Production].[ProductListPriceHistory]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductListPriceHistory]'
ALTER TABLE [Production].[ProductListPriceHistory] NOCHECK CONSTRAINT [FK_ProductListPriceHistory_Product_ProductID]


-- No rows are in ProductListPriceHistory
PRINT 'No rows are in ProductListPriceHistory'

