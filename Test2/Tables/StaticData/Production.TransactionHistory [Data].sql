-- Check Constraint Disable's for Table: [Production].[TransactionHistory]
Print 'Check Constraint Disable''s for Table: [Production].[TransactionHistory]'
ALTER TABLE [Production].[TransactionHistory] NOCHECK CONSTRAINT [CK_TransactionHistory_TransactionType]

-- Foreign Key Constraint Disable's for Table: [Production].[TransactionHistory]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[TransactionHistory]'
ALTER TABLE [Production].[TransactionHistory] NOCHECK CONSTRAINT [FK_TransactionHistory_Product_ProductID]


-- No rows are in TransactionHistory
PRINT 'No rows are in TransactionHistory'

