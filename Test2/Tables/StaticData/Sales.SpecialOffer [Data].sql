-- Check Constraint Disable's for Table: [Sales].[SpecialOffer]
Print 'Check Constraint Disable''s for Table: [Sales].[SpecialOffer]'
ALTER TABLE [Sales].[SpecialOffer] NOCHECK CONSTRAINT [CK_SpecialOffer_DiscountPct]
ALTER TABLE [Sales].[SpecialOffer] NOCHECK CONSTRAINT [CK_SpecialOffer_EndDate]
ALTER TABLE [Sales].[SpecialOffer] NOCHECK CONSTRAINT [CK_SpecialOffer_MaxQty]
ALTER TABLE [Sales].[SpecialOffer] NOCHECK CONSTRAINT [CK_SpecialOffer_MinQty]


-- No rows are in SpecialOffer
PRINT 'No rows are in SpecialOffer'

