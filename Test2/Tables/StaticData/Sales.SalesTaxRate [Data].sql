-- Check Constraint Disable's for Table: [Sales].[SalesTaxRate]
Print 'Check Constraint Disable''s for Table: [Sales].[SalesTaxRate]'
ALTER TABLE [Sales].[SalesTaxRate] NOCHECK CONSTRAINT [CK_SalesTaxRate_TaxType]

-- Foreign Key Constraint Disable's for Table: [Sales].[SalesTaxRate]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesTaxRate]'
ALTER TABLE [Sales].[SalesTaxRate] NOCHECK CONSTRAINT [FK_SalesTaxRate_StateProvince_StateProvinceID]


-- No rows are in SalesTaxRate
PRINT 'No rows are in SalesTaxRate'

