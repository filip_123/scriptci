-- Check Constraint Disable's for Table: [Sales].[SalesOrderDetail]
Print 'Check Constraint Disable''s for Table: [Sales].[SalesOrderDetail]'
ALTER TABLE [Sales].[SalesOrderDetail] NOCHECK CONSTRAINT [CK_SalesOrderDetail_OrderQty]
ALTER TABLE [Sales].[SalesOrderDetail] NOCHECK CONSTRAINT [CK_SalesOrderDetail_UnitPrice]
ALTER TABLE [Sales].[SalesOrderDetail] NOCHECK CONSTRAINT [CK_SalesOrderDetail_UnitPriceDiscount]

-- Foreign Key Constraint Disable's for Table: [Sales].[SalesOrderDetail]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesOrderDetail]'
ALTER TABLE [Sales].[SalesOrderDetail] NOCHECK CONSTRAINT [FK_SalesOrderDetail_SalesOrderHeader_SalesOrderID]
ALTER TABLE [Sales].[SalesOrderDetail] NOCHECK CONSTRAINT [FK_SalesOrderDetail_SpecialOfferProduct_SpecialOfferIDProductID]

-- Trigger Disable's for Table: [Sales].[SalesOrderDetail]
Print 'Trigger Disable''s for Table: [Sales].[SalesOrderDetail]'
ALTER TABLE [Sales].[SalesOrderDetail] DISABLE TRIGGER [iduSalesOrderDetail]


-- No rows are in SalesOrderDetail
PRINT 'No rows are in SalesOrderDetail'

