-- Check Constraint Disable's for Table: [Sales].[SalesPersonQuotaHistory]
Print 'Check Constraint Disable''s for Table: [Sales].[SalesPersonQuotaHistory]'
ALTER TABLE [Sales].[SalesPersonQuotaHistory] NOCHECK CONSTRAINT [CK_SalesPersonQuotaHistory_SalesQuota]

-- Foreign Key Constraint Disable's for Table: [Sales].[SalesPersonQuotaHistory]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesPersonQuotaHistory]'
ALTER TABLE [Sales].[SalesPersonQuotaHistory] NOCHECK CONSTRAINT [FK_SalesPersonQuotaHistory_SalesPerson_BusinessEntityID]


-- No rows are in SalesPersonQuotaHistory
PRINT 'No rows are in SalesPersonQuotaHistory'

