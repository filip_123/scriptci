-- Check Constraint Disable's for Table: [Production].[WorkOrderRouting]
Print 'Check Constraint Disable''s for Table: [Production].[WorkOrderRouting]'
ALTER TABLE [Production].[WorkOrderRouting] NOCHECK CONSTRAINT [CK_WorkOrderRouting_ActualCost]
ALTER TABLE [Production].[WorkOrderRouting] NOCHECK CONSTRAINT [CK_WorkOrderRouting_ActualEndDate]
ALTER TABLE [Production].[WorkOrderRouting] NOCHECK CONSTRAINT [CK_WorkOrderRouting_ActualResourceHrs]
ALTER TABLE [Production].[WorkOrderRouting] NOCHECK CONSTRAINT [CK_WorkOrderRouting_PlannedCost]
ALTER TABLE [Production].[WorkOrderRouting] NOCHECK CONSTRAINT [CK_WorkOrderRouting_ScheduledEndDate]

-- Foreign Key Constraint Disable's for Table: [Production].[WorkOrderRouting]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[WorkOrderRouting]'
ALTER TABLE [Production].[WorkOrderRouting] NOCHECK CONSTRAINT [FK_WorkOrderRouting_Location_LocationID]
ALTER TABLE [Production].[WorkOrderRouting] NOCHECK CONSTRAINT [FK_WorkOrderRouting_WorkOrder_WorkOrderID]


-- No rows are in WorkOrderRouting
PRINT 'No rows are in WorkOrderRouting'

