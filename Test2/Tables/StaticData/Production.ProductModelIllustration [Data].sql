-- Foreign Key Constraint Disable's for Table: [Production].[ProductModelIllustration]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductModelIllustration]'
ALTER TABLE [Production].[ProductModelIllustration] NOCHECK CONSTRAINT [FK_ProductModelIllustration_Illustration_IllustrationID]
ALTER TABLE [Production].[ProductModelIllustration] NOCHECK CONSTRAINT [FK_ProductModelIllustration_ProductModel_ProductModelID]


-- No rows are in ProductModelIllustration
PRINT 'No rows are in ProductModelIllustration'

