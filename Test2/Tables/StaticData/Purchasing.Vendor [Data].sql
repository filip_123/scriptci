-- Check Constraint Disable's for Table: [Purchasing].[Vendor]
Print 'Check Constraint Disable''s for Table: [Purchasing].[Vendor]'
ALTER TABLE [Purchasing].[Vendor] NOCHECK CONSTRAINT [CK_Vendor_CreditRating]

-- Foreign Key Constraint Disable's for Table: [Purchasing].[Vendor]
Print 'Foreign Key Constraint Disable''s for Table: [Purchasing].[Vendor]'
ALTER TABLE [Purchasing].[Vendor] NOCHECK CONSTRAINT [FK_Vendor_BusinessEntity_BusinessEntityID]

-- Trigger Disable's for Table: [Purchasing].[Vendor]
Print 'Trigger Disable''s for Table: [Purchasing].[Vendor]'
ALTER TABLE [Purchasing].[Vendor] DISABLE TRIGGER [dVendor]


-- No rows are in Vendor
PRINT 'No rows are in Vendor'

