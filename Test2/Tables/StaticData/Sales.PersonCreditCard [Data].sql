-- Foreign Key Constraint Disable's for Table: [Sales].[PersonCreditCard]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[PersonCreditCard]'
ALTER TABLE [Sales].[PersonCreditCard] NOCHECK CONSTRAINT [FK_PersonCreditCard_CreditCard_CreditCardID]
ALTER TABLE [Sales].[PersonCreditCard] NOCHECK CONSTRAINT [FK_PersonCreditCard_Person_BusinessEntityID]


-- No rows are in PersonCreditCard
PRINT 'No rows are in PersonCreditCard'

