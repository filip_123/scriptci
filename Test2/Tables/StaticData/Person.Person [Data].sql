-- Check Constraint Disable's for Table: [Person].[Person]
Print 'Check Constraint Disable''s for Table: [Person].[Person]'
ALTER TABLE [Person].[Person] NOCHECK CONSTRAINT [CK_Person_EmailPromotion]
ALTER TABLE [Person].[Person] NOCHECK CONSTRAINT [CK_Person_PersonType]

-- Foreign Key Constraint Disable's for Table: [Person].[Person]
Print 'Foreign Key Constraint Disable''s for Table: [Person].[Person]'
ALTER TABLE [Person].[Person] NOCHECK CONSTRAINT [FK_Person_BusinessEntity_BusinessEntityID]

-- Trigger Disable's for Table: [Person].[Person]
Print 'Trigger Disable''s for Table: [Person].[Person]'
ALTER TABLE [Person].[Person] DISABLE TRIGGER [iuPerson]


-- No rows are in Person
PRINT 'No rows are in Person'

