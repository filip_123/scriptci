/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-04-2017 20.17.29
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESSAUTH

DATABASE:	AdWorks2014
  Fulltext catalogs:  AW2014FullTextCatalog


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

USE [AdWorks2014]
GO

-- Create Fulltext Catalog [AW2014FullTextCatalog]
Print 'Create Fulltext Catalog [AW2014FullTextCatalog]'
GO
CREATE FULLTEXT CATALOG [AW2014FullTextCatalog]
	WITH ACCENT_SENSITIVITY = ON
	AS DEFAULT
	AUTHORIZATION [dbo]
GO

