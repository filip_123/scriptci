-- Foreign Key Constraint Disable's for Table: [Person].[BusinessEntityContact]
Print 'Foreign Key Constraint Disable''s for Table: [Person].[BusinessEntityContact]'
ALTER TABLE [Person].[BusinessEntityContact] NOCHECK CONSTRAINT [FK_BusinessEntityContact_BusinessEntity_BusinessEntityID]
ALTER TABLE [Person].[BusinessEntityContact] NOCHECK CONSTRAINT [FK_BusinessEntityContact_ContactType_ContactTypeID]
ALTER TABLE [Person].[BusinessEntityContact] NOCHECK CONSTRAINT [FK_BusinessEntityContact_Person_PersonID]


-- No rows are in BusinessEntityContact
PRINT 'No rows are in BusinessEntityContact'

