-- Check Constraint Disable's for Table: [Production].[Document]
Print 'Check Constraint Disable''s for Table: [Production].[Document]'
ALTER TABLE [Production].[Document] NOCHECK CONSTRAINT [CK_Document_Status]

-- Foreign Key Constraint Disable's for Table: [Production].[Document]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[Document]'
ALTER TABLE [Production].[Document] NOCHECK CONSTRAINT [FK_Document_Employee_Owner]


-- No rows are in Document
PRINT 'No rows are in Document'

