-- Check Constraint Disable's for Table: [Purchasing].[ProductVendor]
Print 'Check Constraint Disable''s for Table: [Purchasing].[ProductVendor]'
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [CK_ProductVendor_AverageLeadTime]
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [CK_ProductVendor_LastReceiptCost]
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [CK_ProductVendor_MaxOrderQty]
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [CK_ProductVendor_MinOrderQty]
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [CK_ProductVendor_OnOrderQty]
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [CK_ProductVendor_StandardPrice]

-- Foreign Key Constraint Disable's for Table: [Purchasing].[ProductVendor]
Print 'Foreign Key Constraint Disable''s for Table: [Purchasing].[ProductVendor]'
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [FK_ProductVendor_Product_ProductID]
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [FK_ProductVendor_UnitMeasure_UnitMeasureCode]
ALTER TABLE [Purchasing].[ProductVendor] NOCHECK CONSTRAINT [FK_ProductVendor_Vendor_BusinessEntityID]


-- No rows are in ProductVendor
PRINT 'No rows are in ProductVendor'

