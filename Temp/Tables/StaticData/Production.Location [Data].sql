-- Check Constraint Disable's for Table: [Production].[Location]
Print 'Check Constraint Disable''s for Table: [Production].[Location]'
ALTER TABLE [Production].[Location] NOCHECK CONSTRAINT [CK_Location_Availability]
ALTER TABLE [Production].[Location] NOCHECK CONSTRAINT [CK_Location_CostRate]


-- No rows are in Location
PRINT 'No rows are in Location'

