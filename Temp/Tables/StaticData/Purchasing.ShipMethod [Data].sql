-- Check Constraint Disable's for Table: [Purchasing].[ShipMethod]
Print 'Check Constraint Disable''s for Table: [Purchasing].[ShipMethod]'
ALTER TABLE [Purchasing].[ShipMethod] NOCHECK CONSTRAINT [CK_ShipMethod_ShipBase]
ALTER TABLE [Purchasing].[ShipMethod] NOCHECK CONSTRAINT [CK_ShipMethod_ShipRate]


-- No rows are in ShipMethod
PRINT 'No rows are in ShipMethod'

