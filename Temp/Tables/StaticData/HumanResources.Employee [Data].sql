-- Check Constraint Disable's for Table: [HumanResources].[Employee]
Print 'Check Constraint Disable''s for Table: [HumanResources].[Employee]'
ALTER TABLE [HumanResources].[Employee] NOCHECK CONSTRAINT [CK_Employee_BirthDate]
ALTER TABLE [HumanResources].[Employee] NOCHECK CONSTRAINT [CK_Employee_Gender]
ALTER TABLE [HumanResources].[Employee] NOCHECK CONSTRAINT [CK_Employee_HireDate]
ALTER TABLE [HumanResources].[Employee] NOCHECK CONSTRAINT [CK_Employee_MaritalStatus]
ALTER TABLE [HumanResources].[Employee] NOCHECK CONSTRAINT [CK_Employee_SickLeaveHours]
ALTER TABLE [HumanResources].[Employee] NOCHECK CONSTRAINT [CK_Employee_VacationHours]

-- Foreign Key Constraint Disable's for Table: [HumanResources].[Employee]
Print 'Foreign Key Constraint Disable''s for Table: [HumanResources].[Employee]'
ALTER TABLE [HumanResources].[Employee] NOCHECK CONSTRAINT [FK_Employee_Person_BusinessEntityID]

-- Trigger Disable's for Table: [HumanResources].[Employee]
Print 'Trigger Disable''s for Table: [HumanResources].[Employee]'
ALTER TABLE [HumanResources].[Employee] DISABLE TRIGGER [dEmployee]


-- No rows are in Employee
PRINT 'No rows are in Employee'

