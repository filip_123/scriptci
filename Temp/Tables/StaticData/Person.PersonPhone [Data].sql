-- Foreign Key Constraint Disable's for Table: [Person].[PersonPhone]
Print 'Foreign Key Constraint Disable''s for Table: [Person].[PersonPhone]'
ALTER TABLE [Person].[PersonPhone] NOCHECK CONSTRAINT [FK_PersonPhone_Person_BusinessEntityID]
ALTER TABLE [Person].[PersonPhone] NOCHECK CONSTRAINT [FK_PersonPhone_PhoneNumberType_PhoneNumberTypeID]


-- No rows are in PersonPhone
PRINT 'No rows are in PersonPhone'

