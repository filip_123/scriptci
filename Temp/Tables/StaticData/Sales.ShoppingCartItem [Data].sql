-- Check Constraint Disable's for Table: [Sales].[ShoppingCartItem]
Print 'Check Constraint Disable''s for Table: [Sales].[ShoppingCartItem]'
ALTER TABLE [Sales].[ShoppingCartItem] NOCHECK CONSTRAINT [CK_ShoppingCartItem_Quantity]

-- Foreign Key Constraint Disable's for Table: [Sales].[ShoppingCartItem]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[ShoppingCartItem]'
ALTER TABLE [Sales].[ShoppingCartItem] NOCHECK CONSTRAINT [FK_ShoppingCartItem_Product_ProductID]


-- No rows are in ShoppingCartItem
PRINT 'No rows are in ShoppingCartItem'

