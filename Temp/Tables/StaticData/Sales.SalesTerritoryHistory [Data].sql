-- Check Constraint Disable's for Table: [Sales].[SalesTerritoryHistory]
Print 'Check Constraint Disable''s for Table: [Sales].[SalesTerritoryHistory]'
ALTER TABLE [Sales].[SalesTerritoryHistory] NOCHECK CONSTRAINT [CK_SalesTerritoryHistory_EndDate]

-- Foreign Key Constraint Disable's for Table: [Sales].[SalesTerritoryHistory]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesTerritoryHistory]'
ALTER TABLE [Sales].[SalesTerritoryHistory] NOCHECK CONSTRAINT [FK_SalesTerritoryHistory_SalesPerson_BusinessEntityID]
ALTER TABLE [Sales].[SalesTerritoryHistory] NOCHECK CONSTRAINT [FK_SalesTerritoryHistory_SalesTerritory_TerritoryID]


-- No rows are in SalesTerritoryHistory
PRINT 'No rows are in SalesTerritoryHistory'

