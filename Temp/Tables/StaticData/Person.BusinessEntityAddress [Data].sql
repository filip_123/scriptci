-- Foreign Key Constraint Disable's for Table: [Person].[BusinessEntityAddress]
Print 'Foreign Key Constraint Disable''s for Table: [Person].[BusinessEntityAddress]'
ALTER TABLE [Person].[BusinessEntityAddress] NOCHECK CONSTRAINT [FK_BusinessEntityAddress_Address_AddressID]
ALTER TABLE [Person].[BusinessEntityAddress] NOCHECK CONSTRAINT [FK_BusinessEntityAddress_AddressType_AddressTypeID]
ALTER TABLE [Person].[BusinessEntityAddress] NOCHECK CONSTRAINT [FK_BusinessEntityAddress_BusinessEntity_BusinessEntityID]


-- No rows are in BusinessEntityAddress
PRINT 'No rows are in BusinessEntityAddress'

