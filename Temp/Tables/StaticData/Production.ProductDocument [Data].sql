-- Foreign Key Constraint Disable's for Table: [Production].[ProductDocument]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductDocument]'
ALTER TABLE [Production].[ProductDocument] NOCHECK CONSTRAINT [FK_ProductDocument_Document_DocumentNode]
ALTER TABLE [Production].[ProductDocument] NOCHECK CONSTRAINT [FK_ProductDocument_Product_ProductID]


-- No rows are in ProductDocument
PRINT 'No rows are in ProductDocument'

