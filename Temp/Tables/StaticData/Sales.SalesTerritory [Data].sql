-- Check Constraint Disable's for Table: [Sales].[SalesTerritory]
Print 'Check Constraint Disable''s for Table: [Sales].[SalesTerritory]'
ALTER TABLE [Sales].[SalesTerritory] NOCHECK CONSTRAINT [CK_SalesTerritory_CostLastYear]
ALTER TABLE [Sales].[SalesTerritory] NOCHECK CONSTRAINT [CK_SalesTerritory_CostYTD]
ALTER TABLE [Sales].[SalesTerritory] NOCHECK CONSTRAINT [CK_SalesTerritory_SalesLastYear]
ALTER TABLE [Sales].[SalesTerritory] NOCHECK CONSTRAINT [CK_SalesTerritory_SalesYTD]

-- Foreign Key Constraint Disable's for Table: [Sales].[SalesTerritory]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[SalesTerritory]'
ALTER TABLE [Sales].[SalesTerritory] NOCHECK CONSTRAINT [FK_SalesTerritory_CountryRegion_CountryRegionCode]


-- No rows are in SalesTerritory
PRINT 'No rows are in SalesTerritory'

