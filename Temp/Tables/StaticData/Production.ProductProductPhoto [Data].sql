-- Foreign Key Constraint Disable's for Table: [Production].[ProductProductPhoto]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductProductPhoto]'
ALTER TABLE [Production].[ProductProductPhoto] NOCHECK CONSTRAINT [FK_ProductProductPhoto_Product_ProductID]
ALTER TABLE [Production].[ProductProductPhoto] NOCHECK CONSTRAINT [FK_ProductProductPhoto_ProductPhoto_ProductPhotoID]


-- No rows are in ProductProductPhoto
PRINT 'No rows are in ProductProductPhoto'

