-- Check Constraint Disable's for Table: [Production].[ProductCostHistory]
Print 'Check Constraint Disable''s for Table: [Production].[ProductCostHistory]'
ALTER TABLE [Production].[ProductCostHistory] NOCHECK CONSTRAINT [CK_ProductCostHistory_EndDate]
ALTER TABLE [Production].[ProductCostHistory] NOCHECK CONSTRAINT [CK_ProductCostHistory_StandardCost]

-- Foreign Key Constraint Disable's for Table: [Production].[ProductCostHistory]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[ProductCostHistory]'
ALTER TABLE [Production].[ProductCostHistory] NOCHECK CONSTRAINT [FK_ProductCostHistory_Product_ProductID]


-- No rows are in ProductCostHistory
PRINT 'No rows are in ProductCostHistory'

