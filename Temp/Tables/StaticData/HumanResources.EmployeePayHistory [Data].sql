-- Check Constraint Disable's for Table: [HumanResources].[EmployeePayHistory]
Print 'Check Constraint Disable''s for Table: [HumanResources].[EmployeePayHistory]'
ALTER TABLE [HumanResources].[EmployeePayHistory] NOCHECK CONSTRAINT [CK_EmployeePayHistory_PayFrequency]
ALTER TABLE [HumanResources].[EmployeePayHistory] NOCHECK CONSTRAINT [CK_EmployeePayHistory_Rate]

-- Foreign Key Constraint Disable's for Table: [HumanResources].[EmployeePayHistory]
Print 'Foreign Key Constraint Disable''s for Table: [HumanResources].[EmployeePayHistory]'
ALTER TABLE [HumanResources].[EmployeePayHistory] NOCHECK CONSTRAINT [FK_EmployeePayHistory_Employee_BusinessEntityID]


-- No rows are in EmployeePayHistory
PRINT 'No rows are in EmployeePayHistory'

