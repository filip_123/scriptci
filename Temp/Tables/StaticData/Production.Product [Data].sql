-- Check Constraint Disable's for Table: [Production].[Product]
Print 'Check Constraint Disable''s for Table: [Production].[Product]'
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_Class]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_DaysToManufacture]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_ListPrice]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_ProductLine]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_ReorderPoint]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_SafetyStockLevel]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_SellEndDate]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_StandardCost]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_Style]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [CK_Product_Weight]

-- Foreign Key Constraint Disable's for Table: [Production].[Product]
Print 'Foreign Key Constraint Disable''s for Table: [Production].[Product]'
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [FK_Product_ProductModel_ProductModelID]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [FK_Product_ProductSubcategory_ProductSubcategoryID]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [FK_Product_UnitMeasure_SizeUnitMeasureCode]
ALTER TABLE [Production].[Product] NOCHECK CONSTRAINT [FK_Product_UnitMeasure_WeightUnitMeasureCode]


-- No rows are in Product
PRINT 'No rows are in Product'

