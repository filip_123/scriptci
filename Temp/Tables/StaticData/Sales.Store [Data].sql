-- Foreign Key Constraint Disable's for Table: [Sales].[Store]
Print 'Foreign Key Constraint Disable''s for Table: [Sales].[Store]'
ALTER TABLE [Sales].[Store] NOCHECK CONSTRAINT [FK_Store_BusinessEntity_BusinessEntityID]
ALTER TABLE [Sales].[Store] NOCHECK CONSTRAINT [FK_Store_SalesPerson_SalesPersonID]


-- No rows are in Store
PRINT 'No rows are in Store'

